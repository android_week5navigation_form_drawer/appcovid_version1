import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var risk = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดในสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Risk')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: riskValue[0],
              title: Text(risk[0]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[1],
              title: Text(risk[1]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[2],
              title: Text(risk[2]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[3],
              title: Text(risk[3]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[4],
              title: Text(risk[4]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[5],
              title: Text(risk[5]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: riskValue[6],
              title: Text(risk[6]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[6] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('Risk_values') ??
        '[false, false, false, false, false, false, false]';
    print(strRiskValue.substring(1, strRiskValue.length - 1));
    var arrStrRiskValue =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValue.length; i++) {
        riskValue[i] = (arrStrRiskValue[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('Risk_values', riskValue.toString());
  }
}
