import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Questionwidget extends StatefulWidget {
  Questionwidget({Key? key}) : super(key: key);

  @override
  _QuestionwidgetState createState() => _QuestionwidgetState();
}

class _QuestionwidgetState extends State<Questionwidget> {
  var questionValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var question = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกนํ้ามูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValue[0],
              title: Text(question[0]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[1],
              title: Text(question[1]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[2],
              title: Text(question[2]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[3],
              title: Text(question[3]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[4],
              title: Text(question[4]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[5],
              title: Text(question[5]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[6],
              title: Text(question[6]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[7],
              title: Text(question[7]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[8],
              title: Text(question[8]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[9],
              title: Text(question[9]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[10],
              title: Text(question[10]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[10] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionValue[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValue.toString());
  }
}
